FROM python:3.8-slim
RUN mkdir /app
WORKDIR /app
COPY requirements.txt /app/
RUN pip install -U pip \
    && pip install --no-cache-dir -r requirements.txt
ENV PYTHONUNBUFFERED 1
ENV BASE_YEAR 2015
ENV ITER_YEAR 2045
ENV HDF5_USE_FILE_LOCKING=FALSE
ENV DEBUG False
ENV ROOT_FOLDER /var/map
ENV H5 /var/map/simulation_result.h5
COPY Exploration.py /app/
COPY maps /app/maps/
VOLUME [ "/var/map" ]
EXPOSE 8765
CMD ["python", "Exploration.py"]