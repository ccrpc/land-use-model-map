import os
import pandas as pd

from maps import dframe_explorer


def _foreign_index(series1, series2):
    """Returns a foreign index column for the second series
     by looking up in the first series. An example is to insert
     zone_id into buildings table by looking up the parcels table """
    df = pd.merge(
        pd.DataFrame({"left": series2}),
        pd.DataFrame({"right": series1}),
        left_on="left",
        right_index=True,
        how="left",
    )
    return df.right


cwd = os.getcwd()
h5_file = os.getenv("H5", cwd + "/data/simulation_result.h5")
result_hdf = pd.HDFStore(h5_file, mode="r")
iter_year = int(os.getenv("ITER_YEAR", 2019)) + 1
base_year = int(os.getenv("BASE_YEAR", 2015))
years = range(base_year, iter_year)

buildings = {}
buildings_zone_id = {}

households = {}
households_zone_id = {}


group_quarters = {}
group_quarters_zone_id = {}

parcels = {}

for year in years:
    buildings[year] = result_hdf.get(f"/{str(year)}/buildings")
    households[year] = result_hdf.get(f"/{str(year)}/households")
    parcels[year] = result_hdf.get(f"/{str(year)}/parcels")
    group_quarters[year] = result_hdf.get(f"/{str(year)}/group_quarters")

    buildings_zone_id[year] = _foreign_index(
        parcels[year].zone_id, buildings[year].parcel_id
    )
    buildings[year]["zone_id"] = buildings_zone_id[year]

    households_zone_id[year] = _foreign_index(
        buildings[year].zone_id, households[year].building_id
    )
    households[year]["zone_id"] = households_zone_id[year]

    group_quarters_zone_id[year] = _foreign_index(
        parcels[year].zone_id, group_quarters[year].parcel_id
    )
    group_quarters[year]["zone_id"] = group_quarters_zone_id[year]

"""Merge dict using update() method. Changes made in dict1"""


def merge_dict(dict1, dict2):
    return dict1.update(dict2)


d = {"buildings" + "_" + str(year): buildings[year] for year in years}
merge_dict(
    d, {"households" + "_" + str(year): households[year] for year in years}
)
merge_dict(
    d,
    {
        "group_quarters" + "_" + str(year): group_quarters[year]
        for year in years
    },
)


dframe_explorer.start(
    d,
    center=[40.105868, -88.178303],
    zoom=11,
    shape_json="data/zones.json",
    geom_name="TAZ_2",  # from JSON file
    join_name="zone_id",  # from data frames
    precision=2,
    years=f"Array.from(new Array({iter_year-base_year}), (x,i) => i + {base_year})",
)
