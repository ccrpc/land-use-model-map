# Land Use Model Map

A web map application to visualize land use model simulation results for Champaign county 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine. 

### Prerequisites

What things you need to install the software and how to install them

```
- docker
- docker-compose
```

### Installing

Clone this repository

```bash
git clone git@gitlab.com:ccrpc/land-use-model-map.git
```

Run

```bash
cd land-use-model-map
docker-compose up
```

Once the app server is up and running, open a browser and go to [localhost:8765](localhost:8765)


## Authors

Land Use Model map was developed upon [UrbanSim](https://github.com/UDST/urbansim) by **Lei Jin** for the Champaign County Regional
Planning Commission.


## License

This project is licensed under the BSD 3-clause license - see the [LICENSE.md](LICENSE.md) file for details


## Acknowledgments

* UDST, author of [UrbanSim](https://github.com/UDST/urbansim)
